import React from 'react';
import { View, StyleSheet } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import {
  HomeScreen,
  SearchScreen,
  TicketScreen,
  UserAccountScreen,
} from '../pages';

import { COLORS, FONT_SIZE, SPACING } from '../utils/theme/theme';
import CustomIcons from '../components/atoms/CustomIcons/CustomIcons';

const Tab = createBottomTabNavigator();

const TabNavigator = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        tabBarHideOnKeyboard: true,
        headerShown: false,
        tabBarStyle: {
          backgroundColor: COLORS.Black,
          borderTopWidth: 0,
          height: SPACING.space_10 * 10,
        },
      }}>
      <Tab.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{
          tabBarShowLabel: false,
          // eslint-disable-next-line react/no-unstable-nested-components
          tabBarIcon: ({ focused }) => {
            return (
              <View
                style={[
                  styles.activeTabBackGround,
                  focused ? { backgroundColor: COLORS.Orange } : {},
                ]}>
                <CustomIcons
                  name="video"
                  color={COLORS.White}
                  size={FONT_SIZE.size_30}
                />
              </View>
            );
          },
        }}
      />
      <Tab.Screen
        name="SearchScreen"
        component={SearchScreen}
        options={{
          tabBarShowLabel: false,
          // eslint-disable-next-line react/no-unstable-nested-components
          tabBarIcon: ({ focused }) => {
            return (
              <View
                style={[
                  styles.activeTabBackGround,
                  focused ? { backgroundColor: COLORS.Orange } : {},
                ]}>
                <CustomIcons
                  name="search"
                  color={COLORS.White}
                  size={FONT_SIZE.size_30}
                />
              </View>
            );
          },
        }}
      />
      <Tab.Screen
        name="TicketScreen"
        component={TicketScreen}
        options={{
          tabBarShowLabel: false,
          // eslint-disable-next-line react/no-unstable-nested-components
          tabBarIcon: ({ focused }) => {
            return (
              <View
                style={[
                  styles.activeTabBackGround,
                  focused ? { backgroundColor: COLORS.Orange } : {},
                ]}>
                <CustomIcons
                  name="ticket"
                  color={COLORS.White}
                  size={FONT_SIZE.size_30}
                />
              </View>
            );
          },
        }}
      />
      <Tab.Screen
        name="UserAccountScreen"
        component={UserAccountScreen}
        options={{
          tabBarShowLabel: false,
          // eslint-disable-next-line react/no-unstable-nested-components
          tabBarIcon: ({ focused }) => {
            return (
              <View
                style={[
                  styles.activeTabBackGround,
                  focused ? { backgroundColor: COLORS.Orange } : {},
                ]}>
                <CustomIcons
                  name="user"
                  color={COLORS.White}
                  size={FONT_SIZE.size_30}
                />
              </View>
            );
          },
        }}
      />
    </Tab.Navigator>
  );
};

const styles = StyleSheet.create({
  activeTabBackGround: {
    backgroundColor: COLORS.Black,
    padding: SPACING.space_18,
    borderRadius: SPACING.space_18 * 10,
  },
});

export default TabNavigator;
