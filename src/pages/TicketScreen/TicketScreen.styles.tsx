import { StyleSheet } from 'react-native';
import {
  BORDER_RADIUS,
  COLORS,
  FONT_FAMILY,
  FONT_SIZE,
  SPACING,
} from '../../utils/theme/theme';

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    backgroundColor: COLORS.Black,
  },
  appHeaderContainer: {
    marginHorizontal: SPACING.space_36,
    marginTop: SPACING.space_20 * 2,
  },
  ticketContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  ticketBackground: {
    alignSelf: 'center',
    width: 300,
    aspectRatio: 200 / 300,
    borderTopLeftRadius: BORDER_RADIUS.radius_20,
    borderTopRightRadius: BORDER_RADIUS.radius_20,
    overflow: 'hidden',
    justifyContent: 'flex-end',
  },
  linearGradient: {
    height: '70%',
  },
  linear: {
    borderTopColor: COLORS.Black,
    borderTopWidth: 2,
    width: 300,
    alignSelf: 'center',
    backgroundColor: COLORS.Orange,
    borderStyle: 'dashed',
  },
  ticketFooter: {
    backgroundColor: COLORS.Orange,
    width: 300,
    alignItems: 'center',
    paddingBottom: SPACING.space_36,
    alignSelf: 'center',
    borderBottomLeftRadius: BORDER_RADIUS.radius_20,
    borderBottomRightRadius: BORDER_RADIUS.radius_20,
  },
  ticketDateContainer: {
    flexDirection: 'row',
    gap: SPACING.space_36,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: SPACING.space_10,
  },
  dateTitle: {
    fontFamily: FONT_FAMILY.poppins_medium,
    fontSize: FONT_SIZE.size_24,
    color: COLORS.White,
  },
  subTitle: {
    fontFamily: FONT_FAMILY.poppins_regular,
    fontSize: FONT_SIZE.size_14,
    color: COLORS.White,
  },
  subHeading: {
    fontFamily: FONT_FAMILY.poppins_medium,
    fontSize: FONT_SIZE.size_18,
    color: COLORS.White,
  },
  subTitleContainer: {
    alignItems: 'center',
  },
  ticketSeatContainer: {
    flexDirection: 'row',
    gap: SPACING.space_36,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: SPACING.space_10,
  },
  clockIcon: {
    fontSize: FONT_SIZE.size_24,
    color: COLORS.White,
    paddingBottom: SPACING.space_4,
  },
  blackCircle: {
    height: 80,
    width: 80,
    borderRadius: 40,
    backgroundColor: COLORS.Black,
  },
  blackCircleTop: { position: 'absolute', bottom: -40, left: -40 },
  blackCircleBottom: { position: 'absolute', bottom: -40, right: -40 },
  blackCircleTopFooter: { position: 'absolute', top: -40, left: -40 },
  blackCircleBottomFooter: { position: 'absolute', top: -40, right: -40 },
  barcodeImage: {
    height: 50,
    aspectRatio: 158 / 52,
  },
});

export default styles;
