import React, { useEffect, useState } from 'react';
import {
  Alert,
  Image,
  ImageBackground,
  SafeAreaView,
  ScrollView,
  StatusBar,
  Text,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import EncryptedStorage from 'react-native-encrypted-storage';

import styles from './TicketScreen.styles';
import { AppHeader, CustomIcons } from '../../components';
import { COLORS } from '../../utils/theme/theme';

const TicketScreen = ({ navigation, route }: any) => {
  const [ticketData, setTicketData] = useState<Object>(route.params);

  useEffect(() => {
    (async () => {
      try {
        const data = await EncryptedStorage.getItem('ticket');
        if (data) {
          setTicketData(JSON.parse(data));
        }
      } catch (error: any) {
        Alert.alert('Something went wrong while getting ticket data', error);
      }
    })();
  }, []);

  if (ticketData !== undefined && ticketData === null) {
    return (
      <View style={styles.container}>
        <StatusBar hidden />
        <View style={styles.appHeaderContainer}>
          <AppHeader name="arrow-left" onPress={() => navigation.goBack()} />
        </View>
      </View>
    );
  }

  const chosenSeats = ticketData?.seatArray
    .slice(0, 4)
    .map((item: Object, index: number, arr: number[]) => {
      return item + (index === arr.length - 1 ? '' : ', ');
    });

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar hidden />

      <ScrollView>
        <View style={styles.ticketContainer}>
          <ImageBackground
            source={{ uri: ticketData?.ticketImage }}
            style={styles.ticketBackground}>
            <LinearGradient
              colors={[COLORS.OrangeRGB10, COLORS.Orange]}
              style={styles.linearGradient}>
              <View style={[styles.blackCircle, styles.blackCircleTop]} />
              <View style={[styles.blackCircle, styles.blackCircleBottom]} />
            </LinearGradient>
          </ImageBackground>
          <View style={styles.linear} />
          <View style={styles.ticketFooter}>
            <View style={[styles.blackCircle, styles.blackCircleTopFooter]} />
            <View style={[styles.blackCircle, styles.blackCircleBottomFooter]} />
            <View style={styles.ticketDateContainer}>
              <View style={styles.subTitleContainer}>
                <Text style={styles.dateTitle}>{ticketData?.date.date}</Text>
                <Text style={styles.subTitle}>{ticketData?.date.day}</Text>
              </View>
              <View style={styles.subTitleContainer}>
                <CustomIcons name="clock" style={styles.clockIcon} />
                <Text style={styles.subTitle}>{ticketData?.time}</Text>
              </View>
            </View>
            <View style={styles.ticketSeatContainer}>
              <View style={styles.subTitleContainer}>
                <Text style={styles.subHeading}>Hall</Text>
                <Text style={styles.subTitle}>02</Text>
              </View>
              <View style={styles.subTitleContainer}>
                <Text style={styles.subHeading}>Row</Text>
                <Text style={styles.subTitle}>04</Text>
              </View>
              <View style={styles.subTitleContainer}>
                <Text style={styles.subHeading}>Seats</Text>
                <Text style={styles.subTitle}>{chosenSeats}</Text>
              </View>
            </View>
            <Image
              source={require('../../assets/image/barcode.png')}
              style={styles.barcodeImage}
            />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default TicketScreen;
