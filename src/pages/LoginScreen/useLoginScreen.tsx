/**
 *
 * @param opts - Props of LoginScreen
 */
const _handleLogin = (opts: any) => () => {
  opts.navigation.replace('TabNavigator');
};

const _handleCreateNewAccount = (opts: any) => () => {
  opts.navigation.navigate('RegisterScreen');
};

/**
 *
 * @param props - Props of LoginScreen
 */
const useLoginScreen = (props: any) => {
  return {
    handleLogin: _handleLogin(props),
    handleCreateNewAccount: _handleCreateNewAccount(props),
  };
};

export default useLoginScreen;
