import { StyleSheet } from 'react-native';
import { COLORS, FONT_FAMILY, FONT_SIZE } from '../../utils/theme/theme';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.Black,
  },
  subContainer: {
    padding: 40,
    flex: 1,
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 26,
    fontFamily: FONT_FAMILY.poppins_semibold,
    color: COLORS.White,
    marginTop: 40,
    marginBottom: 40,
  },
  link: {
    color: 'white',
    fontSize: FONT_SIZE.size_14,
    fontFamily: FONT_FAMILY.poppins_regular,
    textDecorationLine: 'underline',
  },
  createAcc: {
    color: 'white',
    fontSize: FONT_SIZE.size_16,
    fontFamily: FONT_FAMILY.poppins_regular,
    textDecorationLine: 'underline',
    textAlign: 'center',
  },
});

export default styles;
