import { SafeAreaView, Text, TouchableOpacity, View } from 'react-native';
import React from 'react';
import styles from './LoginScreen.styles';
import { Button, CustomIcons, Gap, InputText } from '../../components';
import { COLORS, FONT_SIZE } from '../../utils/theme/theme';
import useLoginScreen from './useLoginScreen';

/**
 * _renderInputEmail
 *
 * @returns {React.ReactNode} - React Component
 */
const _renderInputEmail = (): React.ReactNode => (
  <>
    <InputText label="Email" />
  </>
);

/**
 * _renderInputPassword
 *
 * @returns {React.ReactNode} - React Component
 */
const _renderInputPassword = (): React.ReactNode => (
  <>
    <InputText secureTextEntry label="Password" />
  </>
);

/**
 * _renderCTA
 *
 * @returns {React.ReactNode} - React Component
 */
const _renderCTA = (): React.ReactNode => (
  <>
    <Text style={styles.title}>Explore the cinema!</Text>
  </>
);

/**
 * _renderForgotPassword
 *
 * @returns {React.ReactNode} - React Component
 */
const _renderForgotPassword = (): React.ReactNode => (
  <TouchableOpacity activeOpacity={0.8}>
    <Text style={styles.link}>Forgot password?</Text>
  </TouchableOpacity>
);

/**
 * _renderCreateAccount
 *
 * @returns {React.ReactNode} - React Component
 */
const _renderCreateAccount = ({
  handleCreateNewAccount,
}: any): React.ReactNode => (
  <TouchableOpacity activeOpacity={0.8} onPress={handleCreateNewAccount}>
    <Text style={styles.createAcc}>Create New Account</Text>
  </TouchableOpacity>
);

/**
 * _renderHeader
 *
 * @returns {React.ReactNode} - React Component
 */
const _renderHeader = (): React.ReactNode => (
  <View>
    <CustomIcons name="video" color={COLORS.White} size={FONT_SIZE.size_50} />
    {_renderCTA()}
    {_renderInputEmail()}
    <Gap height={20} />
    {_renderInputPassword()}
    <Gap height={10} />
    {_renderForgotPassword()}
  </View>
);

/**
 * _renderFooter
 *
 * @returns {React.ReactNode} - React Component
 */
const _renderFooter = (methods: any): React.ReactNode => (
  <View>
    <Button title="Sign In" onPress={methods.handleLogin} />
    <Gap height={20} />
    {_renderCreateAccount(methods)}
  </View>
);

/**
 * _renderBody
 *
 * @returns {React.ReactNode} - React Component
 */
const _renderBody = (methods: any): React.ReactNode => (
  <View style={styles.subContainer}>
    {_renderHeader()}
    {_renderFooter(methods)}
  </View>
);

/**
 * LoginScreen
 *
 * @returns {React.ReactNode} - React Component
 */
const LoginScreen = (props: any): React.ReactNode => {
  const methods = useLoginScreen(props);

  return (
    <SafeAreaView style={styles.container}>{_renderBody(methods)}</SafeAreaView>
  );
};

export default LoginScreen;
