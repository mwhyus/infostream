import { StyleSheet } from 'react-native';
import {
  COLORS,
  FONT_FAMILY,
  FONT_SIZE,
  SPACING,
} from '../../utils/theme/theme';

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    backgroundColor: COLORS.Black,
  },
  picProfile: {
    height: 90,
    width: 90,
    borderRadius: 80,
  },
  profileContainer: {
    alignItems: 'center',
    padding: SPACING.space_36,
    paddingTop: SPACING.space_12,
  },
  picContainer: {
    alignItems: 'center',
    padding: SPACING.space_20,
  },
  profileName: {
    fontFamily: FONT_FAMILY.poppins_medium,
    fontSize: FONT_SIZE.size_16,
    marginTop: SPACING.space_20,
    color: COLORS.White,
  },
  bottomContainer: {
    paddingTop: SPACING.space_12,
    paddingBottom: SPACING.space_20,
    padding: SPACING.space_40,
  },
});

export default styles;
