import React from 'react';
import {
  Image,
  SafeAreaView,
  StatusBar,
  Text,
  View,
  Linking,
  ScrollView,
} from 'react-native';
import styles from './UserAccountScreen.styles';
import { Button, SettingSelection } from '../../components';
import { ILPhoto } from '../../assets/image';

const UserAccountScreen = (props: any) => {
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView bounces={false}>
        <StatusBar hidden />
        <View style={styles.picContainer}>
          <Image style={styles.picProfile} source={ILPhoto} />
          <Text style={styles.profileName}>Armin Artelt</Text>
        </View>

        <View style={styles.profileContainer}>
          <SettingSelection
            icon="user"
            heading="Account"
            subHeading="Edit Profile"
            subTitle="Change Password"
            onPress={() => props.navigation.navigate('UpdateProfileScreen')}
          />
          <SettingSelection
            icon="setting"
            heading="Settings"
            subHeading="Theme"
            subTitle="Permissions"
          />
          <SettingSelection
            icon="dollar"
            heading="Offers & Referrals"
            subHeading="Offer"
            subTitle="Refferals"
          />
          <SettingSelection
            onPress={() => {
              Linking.openURL('https://mwhyus.github.io/');
            }}
            icon="info"
            heading="About"
            subHeading="About Movie App"
            subTitle="More"
          />
        </View>
      </ScrollView>
      <View style={styles.bottomContainer}>
        <Button
          title="Sign Out"
          onPress={() => props.navigation.replace('GetStarted')}
        />
      </View>
    </SafeAreaView>
  );
};

export default UserAccountScreen;
