import { StyleSheet } from 'react-native';
import { COLORS, SPACING } from '../../utils/theme/theme';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.Black,
  },
  appHeaderContainer: {
    padding: SPACING.space_20,
    marginBottom: SPACING.space_36,
  },
  content: {
    padding: SPACING.space_36,
    justifyContent: 'space-between',
    flex: 1,
  },
  buttonContainer: {
    padding: SPACING.space_40,
  },
});

export default styles;
