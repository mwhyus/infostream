import React from 'react';
import { SafeAreaView, ScrollView, View } from 'react-native';
import styles from './RegisterScreen.styles';
import { AppHeader, Button, Gap, InputText } from '../../components';
import { useForm } from '../../utils/';

const RegisterScreen = (props: any) => {
  const [form, setForm] = useForm({
    firstName: '',
    lastName: '',
    email: '',
    password: '',
  });

  const onSubmit = () => {
    props.navigation.navigate('UploadPhotoScreen', form);
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.appHeaderContainer}>
        <AppHeader
          name="arrow-left"
          header="Create Account"
          onPress={() => props.navigation.goBack()}
        />
      </View>
      <ScrollView bounces={false}>
        <View style={styles.content}>
          <View>
            <InputText
              label="First Name"
              value={form.firstName}
              onChangeText={(value: string) => setForm('firstName', value)}
            />
            <Gap height={20} />
            <InputText
              label="Last Name"
              value={form.lastName}
              onChangeText={(value: string) => setForm('lastName', value)}
            />
            <Gap height={20} />
            <InputText
              label="Email"
              value={form.email}
              onChangeText={(value: string) =>
                setForm('email', value.toLowerCase())
              }
            />
            <Gap height={20} />
            <InputText
              label="Password"
              value={form.password}
              secureTextEntry
              onChangeText={(value: string) => setForm('password', value)}
            />
          </View>
        </View>
      </ScrollView>
      <View style={styles.buttonContainer}>
        <Button title="Continue" onPress={onSubmit} />
      </View>
    </SafeAreaView>
  );
};

export default RegisterScreen;
