/* eslint-disable eqeqeq */
import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  Dimensions,
  ActivityIndicator,
  View,
  ScrollView,
  StatusBar,
  FlatList,
} from 'react-native';
import styles from './HomeScreen.styles';
import {
  UPCOMING_MOVIES,
  NOW_PLAYING_MOVIES,
  POPULAR_MOVIES,
  baseImagePath,
} from '../../utils/api/apicalls';
import { COLORS, SPACING } from '../../utils/theme/theme';
import { InputHeader, SubMovieCard, MovieCard } from '../../components';
import { CategoryHeader } from '../../components';

const { width } = Dimensions.get('window');

const getNowPlayingMovies = async () => {
  try {
    let response = await fetch(NOW_PLAYING_MOVIES);
    let json = await response.json();
    return json;
  } catch (error) {
    console.error('Someting went wrong in getNowPlayingMovies', error);
  }
};

const getUpcomingMovies = async () => {
  try {
    let response = await fetch(UPCOMING_MOVIES);
    let json = await response.json();
    return json;
  } catch (error) {
    console.error('Someting went wrong in getUpcomingMovies', error);
  }
};

const getPopularMovies = async () => {
  try {
    let response = await fetch(POPULAR_MOVIES);
    let json = await response.json();
    return json;
  } catch (error) {
    console.error('Someting went wrong in getPopularMovies', error);
  }
};

const HomeScreen = ({ navigation }: any) => {
  const [nowPlayingMovies, setNowPlayingMovies] = useState<any>(undefined);
  const [popularMovies, setPopularMovies] = useState<any>(undefined);
  const [upcomingMovies, setUpcomingMovies] = useState<any>(undefined);

  useEffect(() => {
    (async () => {
      let tempNowPlaying = await getNowPlayingMovies();
      setNowPlayingMovies([
        { id: 'dummy1' },
        ...tempNowPlaying.results,
        { id: 'dummy2' },
      ]);

      let tempPopularMovies = await getPopularMovies();
      setPopularMovies(tempPopularMovies.results);

      let tempUpcomingMovies = await getUpcomingMovies();
      setUpcomingMovies(tempUpcomingMovies.results);
    })();
  }, []);

  const searchMovieFunction = (searchText: string) => {
    navigation.navigate('SearchScreen', { searchText: searchText });
    console.log('searchText', searchText);
  };

  if (!nowPlayingMovies && !popularMovies && !upcomingMovies) {
    return (
      <ScrollView
        bounces={false}
        style={styles.container}
        contentContainerStyle={styles.scrollViewContainer}>
        <StatusBar hidden />

        <SafeAreaView style={styles.inputHeaderContainer}>
          <InputHeader searchFunction={searchMovieFunction} />
        </SafeAreaView>

        <View style={styles.loadingContainer}>
          <ActivityIndicator size="large" color={COLORS.Orange} />
        </View>
      </ScrollView>
    );
  }

  return (
    <ScrollView bounces={false} style={styles.container}>
      <SafeAreaView>
        <StatusBar hidden />

        <CategoryHeader title={'Now Playing'} />
        <FlatList
          showsHorizontalScrollIndicator={false}
          data={nowPlayingMovies}
          horizontal
          keyExtractor={(item: any) => item.id}
          bounces={false}
          snapToInterval={width * 0.7 + SPACING.space_36}
          contentContainerStyle={styles.containerGap36}
          decelerationRate={0}
          renderItem={({ item, index }: any) => {
            if (!item.original_title) {
              return (
                <View
                  style={{
                    width: (width - (width * 0.7 + SPACING.space_36 * 2)) / 2,
                  }}
                />
              );
            }
            return (
              <MovieCard
                shouldMarginatedAtEnd
                onPress={() => {
                  navigation.push('MovieDetailsScreen', {
                    movieId: item.id,
                    canBookTicket: true,
                  });
                }}
                cardWidth={width * 0.7}
                isFirstCard={index == 0 ? true : false}
                isLastCard={index == nowPlayingMovies.length - 1 ? true : false}
                title={item.original_title}
                imagePath={baseImagePath('w780', item.poster_path)}
                genre={item.genre_ids.slice(1, 4)}
                vote_average={Math.ceil(item.vote_average * 2) / 2}
                vote_count={item.vote_count}
              />
            );
          }}
        />
        <CategoryHeader title={'Popular'} />
        <FlatList
          horizontal
          data={popularMovies}
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item: any) => item.id}
          contentContainerStyle={styles.containerGap36}
          renderItem={({ item, index }: any) => (
            <SubMovieCard
              shouldMarginatedAtEnd
              cardFunction={() => {
                navigation.push('MovieDetailsScreen', { movieId: item.id });
              }}
              cardWidth={width / 3}
              isFirstCard={index == 0 ? true : false}
              isLastCard={index == popularMovies.length - 1 ? true : false}
              title={item.original_title}
              imagePath={baseImagePath('w342', item.poster_path)}
            />
          )}
        />
        <CategoryHeader title={'Upcoming'} />
        <FlatList
          horizontal
          data={upcomingMovies}
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item: any) => item.id}
          contentContainerStyle={styles.containerGap36}
          renderItem={({ item, index }: any) => (
            <SubMovieCard
              shouldMarginatedAtEnd
              cardFunction={() => {
                navigation.push('MovieDetailsScreen', { movieId: item.id });
              }}
              cardWidth={width / 3}
              isFirstCard={index == 0 ? true : false}
              isLastCard={index == upcomingMovies.length - 1 ? true : false}
              title={item.original_title}
              imagePath={baseImagePath('w342', item.poster_path)}
            />
          )}
        />
      </SafeAreaView>
    </ScrollView>
  );
};

export default HomeScreen;
