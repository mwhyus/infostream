import { Image, SafeAreaView, TouchableOpacity, View } from 'react-native';
import React from 'react';
import styles from './UpdateProfileScreen.styles';
import { AppHeader, Button, Gap, InputText } from '../../components';
import { ICRemove } from '../../assets/icons';
import { ILPhoto } from '../../assets/image';

const UpdateProfileScreen = (props: any) => {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.appHeaderContainer}>
        <AppHeader
          name="arrow-left"
          header="Edit Profile"
          onPress={() => props.navigation.goBack()}
        />
      </View>
      <TouchableOpacity activeOpacity={0.8} style={styles.profile}>
        <View style={styles.photoContainer}>
          <Image source={ILPhoto} style={styles.photoUser} />
          <ICRemove style={styles.addPhoto} />
        </View>
      </TouchableOpacity>
      <View style={styles.content}>
        <View>
          <Gap height={20} />
          <InputText label="Full Name" />
          <Gap height={20} />
          <InputText label="Email Address" />
          <Gap height={20} />
          <InputText label="Password" />
        </View>
        <View style={styles.buttonContainer}>
          <Button title="Save Profile" />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default UpdateProfileScreen;
