import { StyleSheet } from 'react-native';
import {
  COLORS,
  FONT_FAMILY,
  FONT_SIZE,
  SPACING,
} from '../../utils/theme/theme';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.Black,
  },
  appHeaderContainer: {
    padding: SPACING.space_20,
    marginBottom: SPACING.space_36,
  },
  content: {
    padding: SPACING.space_40,
    paddingTop: 0,
    flex: 1,
    justifyContent: 'space-between',
  },
  profile: {
    alignItems: 'center',
  },
  photoContainer: {
    width: 130,
    height: 130,
    borderWidth: 1,
    borderColor: COLORS.WhiteRGBA32,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 130 / 2,
  },
  photoUser: {
    width: 110,
    height: 110,
    borderRadius: 110 / 2,
  },
  addPhoto: {
    position: 'absolute',
    bottom: 8,
    right: 6,
  },
  userName: {
    color: COLORS.White,
    fontSize: FONT_SIZE.size_24,
    fontFamily: FONT_FAMILY.poppins_medium,
  },
  buttonContainer: {
    paddingBottom: SPACING.space_40,
  },
});

export default styles;
