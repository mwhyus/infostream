import { useEffect } from 'react';

/**
 * _useSplash
 * @param {navigation} navigation - navigate to HomeScreen
 * @returns {void}
 *
 * @private
 */
const _useSplash = (navigation: any): void => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('GetStarted');
    }, 2000);
  }, [navigation]);
};

/**
 * useSplashScreen
 *
 * @param {any} props - props
 * @returns {Methods} - useSplashScreen Methods
 */
const useSplashScreen = ({ navigation }: any) => {
  return {
    useSplash: _useSplash(navigation),
  };
};

export default useSplashScreen;
