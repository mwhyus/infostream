import React from 'react';
import { SafeAreaView, View } from 'react-native';

import styles from './SplashScreen.styles';
import useSplashScreen from './useSplashScreen';
import { ICApp } from '../../assets/icons';

/**
 * renderLogo
 *
 * @returns {React.ReactNode}
 */
const renderLogo = (): React.ReactNode => (
  <View>
    <ICApp />
  </View>
);

/**
 * SplashScreen
 *
 * @param {any} props - props
 * @returns {React.ReactNode}
 */
const SplashScreen = (props: any): React.ReactNode => {
  const { useSplash } = useSplashScreen(props);

  useSplash;

  return <SafeAreaView style={styles.container}>{renderLogo()}</SafeAreaView>;
};

export default SplashScreen;
