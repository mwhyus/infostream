import { StyleSheet } from 'react-native';
import { COLORS, FONT_SIZE, SPACING } from '../../utils/theme/theme';

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.Black,
  },
  title: {
    color: COLORS.White,
    fontSize: FONT_SIZE.size_20,
    fontWeight: '600',
    marginTop: SPACING.space_20,
  },
});

export default styles;
