import { useState, useEffect } from 'react';
import { getMovieDetail, getMovieCastDetail } from '../../utils/api/apicalls';

/**
 * _getMovieDetails
 *
 * @param {movieId} movieId - movieId
 * @returns {void} - void
 *
 * @private
 */
const _getMovieDetails = async (movieId: number): Promise<void> => {
  try {
    let response = await fetch(getMovieDetail(movieId));
    let json = await response.json();

    return json;
  } catch (error) {
    console.error('Someting went wrong in getMovieDetails', error);
  }
};

/**
 * _getMovieCastDetails
 *
 * @param {movieId} movieId - movieId
 * @returns {void} - void
 *
 * @private
 */
const _getMovieCastDetails = async (movieId: number): Promise<any> => {
  try {
    let response = await fetch(getMovieCastDetail(movieId));
    let json = await response.json();

    return json;
  } catch (error) {
    console.error('Someting went wrong in getMovieCastDetails', error);
  }
};

/**
 * _useMovieDetails
 *
 * @param {movieId} movieId - movieId
 * @param {setMovieData} setMovieData - setMovieData
 * @param {setMovieCastData} setMovieCastData - setMovieCastData
 * @returns {void} - void
 *
 * @private
 */
const _useMovieDetails = (
  movieId: number,
  setMovieData: any,
  setMovieCastData: any,
): void => {
  useEffect(() => {
    _getMovieDetails(movieId).then(res => setMovieData(res));
    _getMovieCastDetails(movieId).then(res => setMovieCastData(res.cast));
  }, [movieId, setMovieCastData, setMovieData]);
};

/**
 * useMovieDetailsScreen
 *
 * @param {props} props - props
 * @returns {Object} - Methods of useMovieDetailsScreen
 */
const useMovieDetailsScreen = (props: any) => {
  const [movieData, setMovieData] = useState<any>(undefined);
  const [movieCastData, setMovieCastData] = useState<any>(undefined);
  const { movieId, canBookTicket } = props.route.params;

  _useMovieDetails(movieId, setMovieData, setMovieCastData);

  return {
    movieData,
    movieCastData,
    canBookTicket,
  };
};

export default useMovieDetailsScreen;
