import React from 'react';
import {
  ActivityIndicator,
  FlatList,
  Image,
  ImageBackground,
  SafeAreaView,
  ScrollView,
  StatusBar,
  Text,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import styles from './MovieDetailsScreen.styles';
import { baseImagePath } from '../../utils/api/apicalls';
import { COLORS } from '../../utils/theme/theme';
import {
  AppHeader,
  CategoryHeader,
  CustomIcons,
  ButtonBook,
} from '../../components';
import { CastCard } from '../../components';
import useMovieDetailsScreen from './useMovieDetailsScreen';

const MovieDetailsScreen = (props: any) => {
  const { navigation } = props;
  const methods = useMovieDetailsScreen(props);

  if (!methods.movieData && !methods.movieCastData) {
    return (
      <ScrollView
        bounces={false}
        style={styles.container}
        contentContainerStyle={styles.scrollViewContainer}
        showsVerticalScrollIndicator={false}>
        <View />

        <SafeAreaView style={styles.appHeaderContainer}>
          <AppHeader name="arrow-left" onPress={() => navigation.goBack()} />
        </SafeAreaView>

        <View style={styles.loadingContainer}>
          <ActivityIndicator size="large" color={COLORS.Orange} />
        </View>
      </ScrollView>
    );
  }

  return (
    <ScrollView
      bounces={false}
      style={styles.container}
      showsVerticalScrollIndicator={false}>
      <StatusBar hidden />

      <View>
        <ImageBackground
          source={{
            uri: baseImagePath('w780', methods.movieData?.backdrop_path),
          }}
          style={styles.imageBackground}>
          <LinearGradient
            colors={[COLORS.BlackRGB10, COLORS.Black]}
            style={styles.linearGradient}>
            <SafeAreaView style={styles.appHeaderContainer}>
              <AppHeader
                name="arrow-left"
                onPress={() => navigation.goBack()}
              />
            </SafeAreaView>
          </LinearGradient>
        </ImageBackground>
        <View style={styles.imageBackground}>
          <Image
            source={{
              uri: baseImagePath('w342', methods.movieData?.poster_path),
            }}
            style={styles.posterImage}
          />
        </View>

        <View style={styles.timeContainer}>
          <CustomIcons name="clock" style={styles.clockIcon} />
          <Text style={styles.movieTime}>
            {Math.floor(methods.movieData?.runtime / 60)}h{' '}
            {Math.floor(methods.movieData?.runtime % 60)}m
          </Text>
        </View>
      </View>

      <View>
        <Text style={styles.movieTitle}>
          {methods.movieData?.original_title}
        </Text>
        <View style={styles.genreContainer}>
          <Text style={styles.movieTitle}>
            {methods.movieData?.genres.map((item: any, index: number) => {
              return (
                <View style={styles.genreBox} key={index}>
                  <Text style={styles.genreText}>{item.name}</Text>
                </View>
              );
            })}
          </Text>
        </View>
        <Text style={styles.tagline}>{methods.movieData?.tagline}</Text>
      </View>

      <View style={styles.infoContainer}>
        <View style={styles.rateContainer}>
          <CustomIcons name="star" style={styles.starIcon} />
          <Text style={styles.rating}>
            {methods.movieData?.vote_average.toFixed(1)} (
            {methods.movieData?.vote_count})
          </Text>
          <Text style={styles.movieTime}>
            {methods.movieData?.release_date.substring(8, 10)}{' '}
            {new Date(methods.movieData?.release_date).toLocaleString(
              'default',
              {
                month: 'long',
              },
            )}{' '}
            {methods.movieData?.release_date.substring(0, 4)}
          </Text>
        </View>
        <Text style={styles.description}>{methods.movieData?.overview}</Text>
      </View>
      <View>
        <CategoryHeader title="Top Cast" />
        <FlatList
          showsHorizontalScrollIndicator={false}
          data={methods.movieCastData}
          keyExtractor={(item: any) => item.id}
          horizontal
          contentContainerStyle={styles.containerGap24}
          renderItem={({ item, index }: any) => (
            <CastCard
              shouldMarginatedAtEnd
              cardWidth={80}
              isFirstCard={index === 0 ? true : false}
              isLastCard={
                index === methods.movieCastData.length - 1 ? true : false
              }
              imagePath={baseImagePath('w185', item?.profile_path)}
              title={item?.original_name}
              subTitle={item?.character}
            />
          )}
        />
        {methods.canBookTicket && (
          <ButtonBook
            title="Book Now"
            onPress={() => {
              navigation.navigate('SeatBookingScreen', {
                backGroundImage: baseImagePath(
                  'w780',
                  methods.movieData?.backdrop_path,
                ),
                posterImage: baseImagePath(
                  'original',
                  methods.movieData?.poster_path,
                ),
              });
            }}
          />
        )}
      </View>
    </ScrollView>
  );
};

export default MovieDetailsScreen;
