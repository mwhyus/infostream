import React, { useState } from 'react';
import {
  Alert,
  FlatList,
  ImageBackground,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import styles from './SeatBookingScreen.styles';
import LinearGradient from 'react-native-linear-gradient';
import { COLORS } from '../../utils/theme/theme';
import { AppHeader, CustomIcons } from '../../components';
import { SelectDateTime } from '../../components';
import EncryptedStorage from 'react-native-encrypted-storage';

const timeArray: string[] = ['10:30', '12:30', '14:30', '16:30', '18:30'];

const generateDate = () => {
  const date = new Date();
  let weekday = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  let weekdays = [];

  for (let i = 0; i < 7; i++) {
    let tempDate = {
      date: new Date(date.getTime() + i * 24 * 60 * 60 * 1000).getDate(),
      day: weekday[new Date(date.getTime() + i * 24 * 60 * 60 * 1000).getDay()],
    };
    weekdays.push(tempDate);
  }
  return weekdays;
};

const generateSeats = () => {
  let numRow = 8;
  let numCol = 3;
  let rowArray = [];
  let start = 1;
  let hasReachNine = false;

  for (let i = 0; i < numRow; i++) {
    let columnArray = [];
    for (let j = 0; j < numCol; j++) {
      let seat = {
        id: start,
        taken: Boolean(Math.round(Math.random())),
        isSelected: false,
      };
      columnArray.push(seat);
      start++;
    }
    if (i === 3) {
      numCol += 2;
    }
    if (numCol < 9 && !hasReachNine) {
      numCol += 2;
    } else {
      numCol -= 2;
      hasReachNine = true;
    }
    rowArray.push(columnArray);
  }
  return rowArray;
};

const SeatBookingScreen = ({ navigation, route }: any) => {
  const dateArray = generateDate();
  const [selectedDateIndex, setSelectedDateIndex] = useState<any>();
  const [price, setPrice] = useState<number>(0);

  const [twoDSeatArray, setTwoDSeatArray] = useState(generateSeats());
  const [selectedSeatArray, setSelectedSeatArray] = useState<number[]>([]);
  const [selectedTimeIndex, setSelectedTimeIndex] = useState<any>([]);

  const selectSeat = (index: number, subIndex: number, id: number) => {
    if (!twoDSeatArray[index][subIndex].taken) {
      let array: number[] = [...selectedSeatArray];
      let temp: any[][] = [...twoDSeatArray];

      temp[index][subIndex].isSelected = !temp[index][subIndex].isSelected;

      if (!array.includes(id)) {
        array.push(id);
        setSelectedSeatArray(array);
      } else {
        const tempIndex = array.indexOf(id);
        if (tempIndex > -1) {
          array.splice(tempIndex, 1);
          setSelectedSeatArray(array);
        }
      }
      setPrice(array.length * 5);
      setTwoDSeatArray(temp);
    }
  };

  const purchaseTicket = async () => {
    const passingParameter = {
      seatArray: selectedSeatArray,
      time: timeArray[selectedTimeIndex],
      date: dateArray[selectedDateIndex],
      ticketImage: route.params?.posterImage,
    };

    if (
      selectedSeatArray.length !== 0 &&
      timeArray[selectedTimeIndex] !== undefined &&
      dateArray[selectedDateIndex] !== undefined
    ) {
      try {
        await EncryptedStorage.setItem(
          'ticket',
          JSON.stringify(passingParameter),
        );
        navigation.navigate('TicketScreen', passingParameter);
      } catch (error) {
        console.log('Something went wrong while purchase ticket', error);
      }
    } else {
      Alert.alert('Please select seat, date and time');
    }
  };

  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      bounces={false}
      style={styles.container}>
      <View>
        <ImageBackground
          style={styles.imageBackground}
          source={{ uri: route.params?.backGroundImage }}>
          <LinearGradient
            style={styles.linearGradient}
            colors={[COLORS.BlackRGB10, COLORS.Black]}>
            <SafeAreaView style={styles.appHeaderContainer}>
              <AppHeader
                name="arrow-left"
                onPress={() => navigation.goBack()}
              />
            </SafeAreaView>
          </LinearGradient>
        </ImageBackground>
        <Text style={styles.selectSeats}>Select seat</Text>
      </View>

      <View style={styles.seatContainer}>
        <View style={styles.containerGap20}>
          {twoDSeatArray?.map((item, index) => {
            return (
              <View key={index} style={styles.seatRow}>
                {item?.map((subItem, subIndex) => {
                  return (
                    <TouchableOpacity
                      activeOpacity={0.7}
                      key={subItem.id}
                      onPress={() => {
                        selectSeat(index, subIndex, subItem.id);
                      }}>
                      <CustomIcons
                        name="seat"
                        style={[
                          styles.seatIcon,
                          subItem.taken ? { color: COLORS.Grey } : {},
                          subItem.isSelected ? { color: COLORS.Orange } : {},
                        ]}
                      />
                    </TouchableOpacity>
                  );
                })}
              </View>
            );
          })}
        </View>
        <View style={styles.seatRadioContainer}>
          <View style={styles.radioContainer}>
            <CustomIcons name="radio" style={styles.radioIcon} />
            <Text style={styles.radioText}>Available</Text>
          </View>
          <View style={styles.radioContainer}>
            <CustomIcons
              name="radio"
              style={[styles.radioIcon, { color: COLORS.Grey }]}
            />
            <Text style={styles.radioText}>Taken</Text>
          </View>
          <View style={styles.radioContainer}>
            <CustomIcons
              name="radio"
              style={[styles.radioIcon, { color: COLORS.Orange }]}
            />
            <Text style={styles.radioText}>Selected</Text>
          </View>
        </View>
      </View>

      <View>
        <FlatList
          data={dateArray}
          showsHorizontalScrollIndicator={false}
          horizontal
          bounces={false}
          contentContainerStyle={styles.containerGap24}
          renderItem={({ item, index }: any) => {
            return (
              <SelectDateTime
                onPress={() => setSelectedDateIndex(index)}
                title={item.date}
                subTitle={item.day}
                index={index}
                itemArray={dateArray}
                selectedIndex={selectedDateIndex}
                isDate
              />
            );
          }}
        />
      </View>

      <View style={styles.outerContainer}>
        <FlatList
          data={timeArray}
          showsHorizontalScrollIndicator={false}
          horizontal
          bounces={false}
          contentContainerStyle={styles.containerGap24}
          renderItem={({ item, index }: any) => {
            return (
              <SelectDateTime
                onPress={() => setSelectedTimeIndex(index)}
                title={item}
                index={index}
                itemArray={timeArray}
                selectedIndex={selectedTimeIndex}
              />
            );
          }}
        />
      </View>

      <View style={styles.buttonPriceContainer}>
        <View style={styles.priceContainer}>
          <Text style={styles.totalPrice}>Total Price</Text>
          <Text style={styles.priceText}>$ {price}.00</Text>
        </View>
        <TouchableOpacity
          style={styles.buttonTextContainer}
          activeOpacity={0.7}
          onPress={purchaseTicket}>
          <Text style={styles.buttonText}>Buy Tickets</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

export default SeatBookingScreen;
