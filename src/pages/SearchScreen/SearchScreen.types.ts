export interface Methods {
  searchList: any;
  setSearchText: any;
  searchText: string;
  useSearchMovie: void;
}

export interface Setter {
  (value: any): void;
}
