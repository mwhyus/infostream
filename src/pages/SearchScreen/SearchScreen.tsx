import React from 'react';
import {
  FlatList,
  SafeAreaView,
  StatusBar,
  View,
  Dimensions,
} from 'react-native';

import styles from './SearchScreen.styles';
import useSearchScreen from './useSearchScreen';
import { baseImagePath } from '../../utils/api/apicalls';
import { InputHeader, SubMovieCard } from '../../components';
import { SPACING } from '../../utils/theme/theme';

const { width } = Dimensions.get('window');

const renderSearch = (methods: any) => (
  <SafeAreaView style={styles.inputHeaderContainer}>
    <InputHeader
      searchText={methods.searchText}
      setSearchText={methods.setSearchText}
      withButton={false}
    />
  </SafeAreaView>
);

const renderItem = (props: any, item: any) => (
  <>
    <SubMovieCard
      shouldMarginatedArround
      cardFunction={() => {
        props.navigation.push('MovieDetailsScreen', {
          movieId: item.id,
        });
      }}
      cardWidth={width / 2 - SPACING.space_12 * 2}
      title={item.original_title}
      imagePath={baseImagePath('w342', item.poster_path)}
    />
  </>
);

const renderFlatList = (props: any, methods: any) => (
  <View>
    <FlatList
      showsVerticalScrollIndicator={false}
      data={methods.searchList}
      numColumns={2}
      ListHeaderComponent={renderSearch(methods)}
      keyExtractor={(item: any) => item.id}
      contentContainerStyle={styles.centerContainer}
      renderItem={({ item }: any) => renderItem(props, item)}
    />
  </View>
);

const SearchScreen = (props: any) => {
  const methods = useSearchScreen();

  methods.useSearchMovie;

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar hidden />
      {renderFlatList(props, methods)}
    </SafeAreaView>
  );
};

export default SearchScreen;
