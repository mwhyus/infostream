import { useEffect, useState } from 'react';
import { useDebounce } from 'use-debounce';

import { searchMovie } from '../../utils/api/apicalls';
import { Methods, Setter } from './SearchScreen.types';

/**
 * searchMovieFunction
 *
 * @param {debounceValue} debounceValue - debounce function
 * @param {setSearchList} setSearchList - set search list
 * @returns {Promise<void>} - returns a promise
 * @private
 */
const searchMovieFunction = async (
  debounceValue: string,
  setSearchList: Setter,
): Promise<void> => {
  try {
    let response = await fetch(searchMovie(debounceValue));
    let json = await response.json();

    setSearchList(json.results);
  } catch (error) {
    console.error('Someting went wrong in searchMovieFunction', error);
  }
};

/**
 * _useSearchMovie
 *
 * @param {string} searchText - search text
 * @param {setSearchList} setSearchList - set search list
 * @param {debounceValue} debounceValue - debounce function
 * @returns {void} - returns void
 * @private
 */
const _useSearchMovie = (
  searchText: string,
  setSearchList: Setter,
  debounceValue: string,
): void => {
  useEffect(() => {
    searchMovieFunction(debounceValue, setSearchList);
  }, [debounceValue, searchText, setSearchList]);
};

/**
 * useSearchScreen
 *
 * @returns {Methods} - returns an object
 */
const useSearchScreen = (): Methods => {
  const [searchList, setSearchList] = useState([]);
  const [searchText, setSearchText] = useState('');

  const [debounceValue] = useDebounce(searchText, 1500);

  return {
    searchList,
    setSearchText,
    searchText,
    useSearchMovie: _useSearchMovie(searchText, setSearchList, debounceValue),
  };
};

export default useSearchScreen;
