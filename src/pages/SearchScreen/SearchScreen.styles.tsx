import { StyleSheet, Dimensions } from 'react-native';
import { COLORS, SPACING } from '../../utils/theme/theme';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    width,
    alignItems: 'center',
    backgroundColor: COLORS.Black,
  },
  inputHeaderContainer: {
    display: 'flex',
    marginHorizontal: SPACING.space_36,
    marginTop: SPACING.space_28,
    marginBottom: SPACING.space_28 - SPACING.space_12,
  },
  containerGap36: {
    gap: SPACING.space_36,
  },
  centerContainer: {
    alignItems: 'center',
  },
});

export default styles;
