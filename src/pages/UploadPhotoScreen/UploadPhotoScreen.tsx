import {
  Image,
  SafeAreaView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import styles from './UploadPhotoScreen.styles';
import { AppHeader, Button, Gap } from '../../components';
import { ILNullUser } from '../../assets/image';
import { ICAdd } from '../../assets/icons';

const UploadPhotoScreen = (props: any) => {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.appHeaderContainer}>
        <AppHeader
          name="arrow-left"
          header="Upload Photo"
          onPress={() => props.navigation.goBack()}
        />
      </View>
      <View style={styles.content}>
        <View style={styles.profile}>
          <View style={styles.photoContainer}>
            <Image source={ILNullUser} style={styles.photoUser} />
            <ICAdd style={styles.addPhoto} />
          </View>
          <Text style={styles.userName}>Armin Artelt</Text>
        </View>
        <View>
          <Button
            title="Upload and Continue"
            onPress={() => props.navigation.replace('TabNavigator')}
          />
          <Gap height={20} />
          <TouchableOpacity activeOpacity={0.8}>
            <Text style={styles.skipProcess}>Skip for this</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default UploadPhotoScreen;
