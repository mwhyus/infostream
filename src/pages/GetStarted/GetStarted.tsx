import React from 'react';
import { ImageBackground, Text, View } from 'react-native';

import styles from './GetStarted.styles';
import { Button, CustomIcons, Gap } from '../../components';
import { COLORS, FONT_SIZE } from '../../utils/theme/theme';
import { ILGetStarted } from '../../assets/image';
import useGetStarted from './useGetStarted';

/**
 * _renderTitle
 *
 * @returns {React.ReactNode} - React Component
 */
const _renderTitle = (): React.ReactNode => (
  <View style={styles.titleContainer}>
    <Text style={styles.title}>Explore the World of Cinema Today!</Text>
  </View>
);

/**
 * _renderHeader
 *
 * @returns {React.ReactNode} - React Component
 */
const _renderHeader = (): React.ReactNode => (
  <View style={styles.subUpperContainer}>
    <CustomIcons name="video" color={COLORS.White} size={FONT_SIZE.size_50} />
    {_renderTitle()}
  </View>
);

/**
 * _renderFooter
 *
 * @returns {React.ReactNode} - React Component
 */
const _renderFooter = ({
  handlePrimaryButton,
  handleSecondaryButton,
}: any): React.ReactNode => (
  <View>
    <Button type="primary" title="Get Started" onPress={handlePrimaryButton} />
    <Gap height={16} />
    <Button type="secondary" title="Sign In" onPress={handleSecondaryButton} />
  </View>
);

/**
 * _renderBody
 *
 * @param methods - methods of GetStarted
 * @returns {React.ReactNode} - React Component
 */
const _renderBody = (methods: any): React.ReactNode => (
  <View style={styles.container}>
    {_renderHeader()}
    {_renderFooter(methods)}
  </View>
);

/**
 * GetStarted
 *
 * @returns {React.ReactNode} - React Component
 */
const GetStarted = (props: any): React.ReactNode => {
  const methods = useGetStarted(props);

  return (
    <ImageBackground source={ILGetStarted} style={styles.imgBackground}>
      {_renderBody(methods)}
    </ImageBackground>
  );
};

export default GetStarted;
