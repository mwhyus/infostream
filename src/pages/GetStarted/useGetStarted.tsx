/**
 * useGetStarted
 * @param {any} navigation - props
 * @returns {Methods} - useGetStarted Methods
 */
const _handlePrimaryButton =
  (navigation: any): Object =>
  (): void => {
    navigation.navigate('RegisterScreen');
  };

/**
 * useGetStarted
 * @param {any} navigation - props
 * @returns {Methods} - useGetStarted Methods
 */
const _handleSecondaryButton =
  (navigation: any): Object =>
  (): void => {
    navigation.navigate('LoginScreen');
  };

/**
 * useGetStarted
 * @param {any} props - props
 * @returns {Methods} - useGetStarted Methods
 */
const useGetStarted = (props: any): Object => {
  return {
    handlePrimaryButton: _handlePrimaryButton(props.navigation),
    handleSecondaryButton: _handleSecondaryButton(props.navigation),
  };
};

export default useGetStarted;
