import { StyleSheet } from 'react-native';
import { COLORS, FONT_SIZE } from '../../utils/theme/theme';

const styles = StyleSheet.create({
  imgBackground: {
    flex: 1,
  },
  container: {
    flex: 1,
    padding: 40,
    justifyContent: 'space-between',
  },
  subUpperContainer: {
    marginVertical: 40,
  },
  titleContainer: {
    marginTop: 100,
  },
  title: {
    fontSize: FONT_SIZE.size_30,
    fontWeight: '600',
    marginVertical: 20,
    color: COLORS.White,
  },
});

export default styles;
