import HomeScreen from './HomeScreen/HomeScreen';
import MovieDetailsScreen from './MovieDetailsScreen/MovieDetailsScreen';
import SearchScreen from './SearchScreen/SearchScreen';
import SeatBookingScreen from './SeatBookingScreen/SeatBookingScreen';
import TicketScreen from './TicketScreen/TicketScreen';
import UserAccountScreen from './UserAccountScreen/UserAccountScreen';
import SplashScreen from './SplashScreen/SplashScreen';
import GetStarted from './GetStarted/GetStarted';
import LoginScreen from './LoginScreen/LoginScreen';
import RegisterScreen from './RegisterScreen/RegisterScreen';
import UploadPhotoScreen from './UploadPhotoScreen/UploadPhotoScreen';
import UpdateProfileScreen from './UpdateProfileScreen/UpdateProfileScreen';

export {
  HomeScreen,
  MovieDetailsScreen,
  SearchScreen,
  SeatBookingScreen,
  TicketScreen,
  UserAccountScreen,
  SplashScreen,
  GetStarted,
  LoginScreen,
  RegisterScreen,
  UploadPhotoScreen,
  UpdateProfileScreen,
};
