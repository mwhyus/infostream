import { Text, TouchableOpacity, View } from 'react-native';
import React from 'react';
import styles from './ButtonBook.styles';

const ButtonBook = (props: any) => {
  return (
    <View style={styles.buttonBackground}>
      <TouchableOpacity activeOpacity={0.7} onPress={props.onPress}>
        <Text style={styles.buttonText}>{props.title}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default ButtonBook;
