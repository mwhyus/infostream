import { StyleSheet } from 'react-native';
import {
  BORDER_RADIUS,
  COLORS,
  SPACING,
  FONT_FAMILY,
  FONT_SIZE,
} from '../../../utils/theme/theme';

const styles = StyleSheet.create({
  buttonBackground: {
    alignItems: 'center',
    marginVertical: SPACING.space_24,
  },
  buttonText: {
    borderRadius: BORDER_RADIUS.radius_25 * 2,
    paddingHorizontal: SPACING.space_24,
    paddingVertical: SPACING.space_10,
    backgroundColor: COLORS.Orange,
    fontFamily: FONT_FAMILY.poppins_medium,
    fontSize: FONT_SIZE.size_14,
    color: COLORS.White,
  },
});

export default styles;
