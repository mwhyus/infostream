import { View } from 'react-native';
import React from 'react';
import { Props } from './Gap.types';

const Gap = ({ height, width }: Props) => {
  return <View style={{ height: height, width: width }} />;
};

export default Gap;
