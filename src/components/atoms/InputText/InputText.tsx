// @ts-nocheck
import React from 'react';
import { Text, TextInput, View } from 'react-native';
import styles from './InputText.styles';
import useInputText from './useInputText';

/**
 * _renderTextInput
 *
 * @param opts - props of InputText
 * @param methods - methods of InputText
 * @returns {React.ReactNode}
 * @private
 */
const _renderTextInput = (opts: any, methods: any): React.ReactNode => {
  const { value, onChangeText, secureTextEntry } = opts;
  const { onFocus, onBlur, borderColor } = methods;

  return (
    <TextInput
      onFocus={onFocus}
      onBlur={onBlur}
      style={styles.input(borderColor)}
      value={value}
      onChangeText={onChangeText}
      secureTextEntry={secureTextEntry}
    />
  );
};

/**
 * _renderContent
 *
 * @param props - props label of InputText
 * @param methods - methods of InputText
 * @returns {React.ReactNode}
 * @private
 */
const _renderContent = (opts: any, methods: any): React.ReactNode => {
  return (
    <View>
      <Text style={styles.label}>{opts.label}</Text>
      {_renderTextInput(opts, methods)}
    </View>
  );
};

/**
 * InputText
 *
 * @param props - props label of InputText
 * @returns {React.ReactNode}
 */
const InputText = (props: any): React.ReactNode => {
  const methods = useInputText();

  return <View style={styles.container}>{_renderContent(props, methods)}</View>;
};

export default InputText;
