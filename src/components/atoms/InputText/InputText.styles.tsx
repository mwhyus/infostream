// @ts-nocheck
import { StyleSheet } from 'react-native';
import {
  BORDER_RADIUS,
  COLORS,
  FONT_FAMILY,
  FONT_SIZE,
} from '../../../utils/theme/theme';

const styles = StyleSheet.create({
  container: {},
  input: (borderColor: string) => ({
    borderRadius: BORDER_RADIUS.radius_10,
    borderColor: borderColor,
    borderWidth: 1,
    color: COLORS.White,
    padding: 12,
  }),
  label: {
    color: COLORS.White,
    fontSize: FONT_SIZE.size_16,
    marginBottom: 6,
    fontFamily: FONT_FAMILY.poppins_medium,
  },
});

export default styles;
