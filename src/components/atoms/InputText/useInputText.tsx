import { useState } from 'react';
import { COLORS } from '../../../utils/theme/theme';

/**
 * _onFocus
 *
 * @param {any} setBorderColor - set border color
 * @returns {void}
 * @private
 */
const _onFocus = (setBorderColor: any) => () => {
  setBorderColor(COLORS.Orange);
};

/**
 * _onBlur
 *
 * @param {any} setBorderColor - set border color
 * @returns {void}
 * @private
 */
const _onBlur = (setBorderColor: any) => () => {
  setBorderColor(COLORS.WhiteRGBA50);
};

/**
 * useInputText
 *
 * @returns {Methods} - useInputText Methods
 *
 */
const useInputText = () => {
  const [borderColor, setBorderColor] = useState(COLORS.WhiteRGBA50);

  return {
    borderColor,
    onFocus: _onFocus(setBorderColor),
    onBlur: _onBlur(setBorderColor),
  };
};

export default useInputText;
