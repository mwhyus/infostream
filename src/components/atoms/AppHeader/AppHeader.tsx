import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';

import styles from './AppHeader.styles';
import CustomIcons from '../CustomIcons/CustomIcons';

const AppHeader = (props: any) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={props.onPress}
        style={styles.iconBackground}>
        <CustomIcons name={props.name} style={styles.iconStyle} />
      </TouchableOpacity>
      <Text style={styles.headerText}>{props.header}</Text>
      <View style={styles.emptyContainer} />
    </View>
  );
};

export default AppHeader;
