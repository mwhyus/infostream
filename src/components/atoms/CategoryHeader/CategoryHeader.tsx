import React from 'react';
import { Text } from 'react-native';
import styles from './CategoryHeader.styles';

const CategoryHeader = ({ title }: any) => {
  return <Text style={styles.text}>{title}</Text>;
};

export default CategoryHeader;
