// @ts-nocheck
import { StyleSheet } from 'react-native';
import { BORDER_RADIUS, COLORS, FONT_SIZE } from '../../../utils/theme/theme';

const styles = StyleSheet.create({
  container: (type: string) => ({
    backgroundColor: type === 'secondary' ? 'white' : COLORS.Orange,
    paddingVertical: 10,
    borderRadius: BORDER_RADIUS.radius_10,
  }),
  primaryContainer: {
    backgroundColor: COLORS.Orange,
  },
  secondaryContainer: {
    backgroundColor: 'white',
  },
  text: (type: string) => ({
    fontSize: FONT_SIZE.size_16,
    fontWeight: '600',
    textAlign: 'center',
    color: type === 'secondary' ? COLORS.Black : COLORS.White,
  }),
});

export default styles;
