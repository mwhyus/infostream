// @ts-nocheck
import { Text, TouchableOpacity } from 'react-native';
import React from 'react';
import styles from './Button.styles';

const Button = ({ type, title, onPress }: any) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={0.8}
      style={styles.container(type)}>
      <Text style={styles.text(type)}>{title}</Text>
    </TouchableOpacity>
  );
};

export default Button;
