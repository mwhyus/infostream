import React from 'react';
import { Image, SafeAreaView, Text } from 'react-native';

import styles from './CastCard.styles';
import { SPACING } from '../../../utils/theme/theme';

const CastCard = (props: any) => {
  return (
    <SafeAreaView
      style={[
        styles.container,
        props.shouldMarginatedAtEnd
          ? props.isFirstCard
            ? { marginLeft: SPACING.space_24 }
            : props.isLastCard
            ? { marginRight: SPACING.space_24 }
            : {}
          : {},
        { maxWidth: props.cardWidth },
      ]}>
      <Image
        source={{ uri: props.imagePath }}
        style={[styles.cardImage, { width: props.cardWidth }]}
      />
      <Text style={styles.title} numberOfLines={1}>
        {props.title}
      </Text>
      <Text style={styles.subTitle} numberOfLines={1}>
        {props.subTitle}
      </Text>
    </SafeAreaView>
  );
};

export default CastCard;
