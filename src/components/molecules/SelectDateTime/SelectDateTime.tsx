import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import styles from './SelectDateTime.styles';
import { COLORS, SPACING } from '../../../utils/theme/theme';

const SelectDateTime = (props: any) => {
  const { title, subTitle, index, onPress, itemArray, selectedIndex, isDate } =
    props;

  return (
    <TouchableOpacity key={index} activeOpacity={0.7} onPress={onPress}>
      <View
        style={[
          isDate ? styles.dateContainer : styles.timeContainer,
          index === 0
            ? { marginLeft: SPACING.space_24 }
            : index === itemArray.length - 1
            ? { marginRight: SPACING.space_24 }
            : {},
          index === selectedIndex ? { backgroundColor: COLORS.Orange } : {},
        ]}>
        <Text style={styles.title}>{title}</Text>
        {subTitle && <Text style={styles.subTitle}>{subTitle}</Text>}
      </View>
    </TouchableOpacity>
  );
};

export default SelectDateTime;
