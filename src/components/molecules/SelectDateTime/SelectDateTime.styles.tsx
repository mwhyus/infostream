import { StyleSheet } from 'react-native';
import {
  COLORS,
  FONT_FAMILY,
  FONT_SIZE,
  SPACING,
} from '../../../utils/theme/theme';

const styles = StyleSheet.create({
  dateContainer: {
    width: SPACING.space_10 * 7,
    height: SPACING.space_10 * 10,
    borderRadius: SPACING.space_10 * 10,
    backgroundColor: COLORS.DarkGrey,
    alignItems: 'center',
    justifyContent: 'center',
  },
  timeContainer: {
    paddingVertical: SPACING.space_10,
    paddingHorizontal: SPACING.space_20,
    borderRadius: SPACING.space_20,
    backgroundColor: COLORS.Grey,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontFamily: FONT_FAMILY.poppins_medium,
    fonstSize: FONT_SIZE.size_24,
    color: COLORS.White,
  },
  subTitle: {
    fontFamily: FONT_FAMILY.poppins_regular,
    fonstSize: FONT_SIZE.size_12,
    color: COLORS.White,
  },
});

export default styles;
