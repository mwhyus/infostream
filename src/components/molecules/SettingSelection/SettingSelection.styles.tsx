import { StyleSheet } from 'react-native';
import {
  COLORS,
  FONT_FAMILY,
  FONT_SIZE,
  SPACING,
} from '../../../utils/theme/theme';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: SPACING.space_20,
  },
  iconStyle: {
    color: COLORS.White,
    fontSize: FONT_SIZE.size_24,
    paddingHorizontal: SPACING.space_20,
  },
  settingContainer: {
    flex: 1,
  },
  title: {
    color: COLORS.White,
    fontFamily: FONT_FAMILY.poppins_medium,
    fontSize: FONT_SIZE.size_18,
  },
  subTitle: {
    color: COLORS.WhiteRGBA50,
    fontFamily: FONT_FAMILY.poppins_medium,
    fontSize: FONT_SIZE.size_14,
  },
  iconBackGround: {
    justifyContent: 'center',
  },
});

export default styles;
