import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import styles from './SettingSelection.styles';
import { CustomIcons } from '../../atoms';

const SettingSelection = (props: any) => {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      activeOpacity={0.7}
      style={styles.container}>
      <View>
        <CustomIcons name={props.icon} style={styles.iconStyle} />
      </View>
      <View style={styles.settingContainer}>
        <Text style={styles.title}>{props.heading}</Text>
        <Text style={styles.subTitle}>{props.subHeading}</Text>
        <Text style={styles.subTitle}>{props.subTitle}</Text>
      </View>
      <View style={styles.iconBackGround}>
        <CustomIcons name="arrow-right" style={styles.iconStyle} />
      </View>
    </TouchableOpacity>
  );
};

export default SettingSelection;
