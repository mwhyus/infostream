import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';

import styles from './SubMovieCard.styles';
import { SPACING } from '../../../utils/theme/theme';

const SubMovieCard = (props: any) => {
  return (
    <TouchableOpacity onPress={() => props.cardFunction()} activeOpacity={0.7}>
      <View
        style={[
          styles.container,
          props.shouldMarginatedAtEnd
            ? props.isFirstCard
              ? { marginLeft: SPACING.space_36 }
              : props.isLastCard
              ? { marginRight: SPACING.space_36 }
              : {}
            : {},
          props.shouldMarginatedArround ? { margin: SPACING.space_12 } : {},
          { maxWidth: props.cardWidth },
        ]}>
        <Image
          style={[styles.cardImage, { width: props.cardWidth }]}
          source={{ uri: props.imagePath }}
        />
        <Text numberOfLines={1} style={styles.textTitle}>
          {props.title}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default SubMovieCard;
