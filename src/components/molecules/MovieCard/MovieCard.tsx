import React from 'react';
import {
  Image,
  SafeAreaView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { SPACING } from '../../../utils/theme/theme';
import styles from './MovieCard.styles';
import { CustomIcons } from '../../atoms';

const genres: any = {
  28: 'Action',
  12: 'Adventure',
  16: 'Animation',
  35: 'Comedy',
  80: 'Crime',
  99: 'Documentary',
  18: 'Drama',
  10751: 'Family',
  14: 'Fantasy',
  36: 'History',
  27: 'Horror',
  10402: 'Music',
  9648: 'Mystery',
  10749: 'Romance',
  878: 'Science Fiction',
  10770: 'TV Movie',
  53: 'Thriller',
  10752: 'War',
  37: 'Western',
};

const MovieCard = (props: any) => {
  return (
    <SafeAreaView>
      <View
        style={[
          styles.container,
          props.shouldMarginatedAtEnd
            ? props.isFirstCard
              ? { marginLeft: SPACING.space_36 }
              : props.isLastCard
              ? { marginRight: SPACING.space_36 }
              : {}
            : {},
          props.shouldMarginatedArround ? { margin: SPACING.space_12 } : {},
          { maxWidth: props.cardWidth },
        ]}>
        <TouchableOpacity onPress={props.onPress} activeOpacity={0.7}>
          <Image
            style={[styles.cardImage, { width: props.cardWidth }]}
            source={{ uri: props.imagePath }}
          />
        </TouchableOpacity>
        <View>
          <View style={styles.rateContainer}>
            <CustomIcons name="star" style={styles.starIcon} />
            <Text style={styles.rating}>
              {props.vote_average} ({props.vote_count})
            </Text>
          </View>

          <Text numberOfLines={1} style={styles.textTitle}>
            {props.title}
          </Text>

          <View style={styles.genreContainer}>
            {props.genre.map((item: any) => {
              return (
                <View key={item} style={styles.genreBox}>
                  <Text style={styles.genreText}>{genres[item]}</Text>
                </View>
              );
            })}
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default MovieCard;
