import React from 'react';
import { TextInput, SafeAreaView, TouchableOpacity } from 'react-native';

import styles from './InputHeader.styles';
import { COLORS, FONT_SIZE } from '../../../utils/theme/theme';
import { CustomIcons } from '../../atoms/';

/**
 * textInputComponent
 * @param {any} setSearchText - function to set search text
 *
 * @returns {React.ReactNode}
 */
const textInputComponent = (setSearchText: any): React.ReactNode => {
  return (
    <TextInput
      style={styles.textInput}
      onChangeText={textInput => setSearchText(textInput)}
      placeholder="Search movies..."
      placeholderTextColor={COLORS.WhiteRGBA32}
    />
  );
};

/**
 * buttonSearchComponent
 * @param {string} searchText - text to search
 * @param {any} searchFunction - function to search
 *
 * @returns {React.ReactNode}
 */
const buttonSearchComponent = (
  searchText: string,
  searchFunction: any,
): React.ReactNode => {
  return (
    <TouchableOpacity
      style={styles.searchIcon}
      onPress={() => searchFunction(searchText)}>
      <CustomIcons
        name="search"
        color={COLORS.Orange}
        size={FONT_SIZE.size_20}
      />
    </TouchableOpacity>
  );
};

/**
 * InputHeader
 *
 * @param {any} props - props
 * @returns {React.ReactNode}
 */
const InputHeader = (props: any): React.ReactNode => {
  const { searchText, setSearchText, withButton } = props;

  return (
    <SafeAreaView style={styles.inputBox}>
      {textInputComponent(setSearchText)}
      {withButton && buttonSearchComponent(searchText, props.searchFunction)}
    </SafeAreaView>
  );
};

export default InputHeader;
