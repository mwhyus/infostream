import { useState } from 'react';

type FormValues = {
  [key: string]: string;
};

const useForm = (initialValues: FormValues) => {
  const [values, setValues] = useState<any>(initialValues);

  return [
    values,
    (formType: string, formValue: string) => {
      return setValues({ ...values, [formType]: formValue });
    },
  ];
};

export default useForm;
