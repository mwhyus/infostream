const apikey: string = 'c7a489202e1d36ec93904953c6221c47';

export const NOW_PLAYING_MOVIES: string = `https://api.themoviedb.org/3/movie/now_playing?api_key=${apikey}`;
export const UPCOMING_MOVIES: string = `https://api.themoviedb.org/3/movie/upcoming?api_key=${apikey}`;
export const POPULAR_MOVIES: string = `https://api.themoviedb.org/3/movie/popular?api_key=${apikey}`;

export const baseImagePath = (size: string, path: string) => {
  return `https://image.tmdb.org/t/p/${size}${path}`;
};

export const searchMovie = (keyword: string) => {
  return `https://api.themoviedb.org/3/search/movie?api_key=${apikey}&query=${keyword}`;
};

export const getMovieDetail = (id: number) => {
  return `https://api.themoviedb.org/3/movie/${id}?api_key=${apikey}`;
};

export const getMovieCastDetail = (id: number) => {
  return `https://api.themoviedb.org/3/movie/${id}/credits?api_key=${apikey}`;
};
