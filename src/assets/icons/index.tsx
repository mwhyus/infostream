import ICAdd from './plus.svg';
import ICRemove from './remove.svg';
import ICApp from './mobox.svg';

export { ICAdd, ICRemove, ICApp };
