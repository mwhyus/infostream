import ILGetStarted from './cover.png';
import ILNullUser from './null_user.png';
import ILPhoto from './Illenium.jpeg';

export { ILGetStarted, ILNullUser, ILPhoto };
